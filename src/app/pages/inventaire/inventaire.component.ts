import { Component } from '@angular/core';

@Component({
  selector: 'app-inventaire',
  templateUrl: './inventaire.component.html',
  styleUrls: ['./inventaire.component.scss']
})

export class InventaireComponent {
  nom = "Kadian";
  description = "Ce médicament est un analgésique narcotique. Habituellement, on l'utilise pour la douleur. On peut sentir son action en quelques heures."
  concentration = "10mg"
}
