import { Component, Input } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})

export class NavigationComponent {
  usager: string = "usager";

  constructor(private router: Router) {}

  goToPath(path: string) {
    this.router.navigate([path]);
  }
}
