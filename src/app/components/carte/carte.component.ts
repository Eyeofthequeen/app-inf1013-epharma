import { KeyValue } from '@angular/common';
import { Component, Input } from '@angular/core';
import { TypeMedication } from 'src/app/constantes';


@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.scss']
})

export class CarteComponent {
  @Input() nom: string = "";
  @Input() type: TypeMedication = 0;
  @Input() concentration: string = "";
  @Input() description: string = "";
  @Input() quantite: number = 0;
  @Input() prix: number = 0;

  private types: KeyValue<TypeMedication, string>[] = [
    {key: TypeMedication.Capsule, value: "capsule"},
    {key: TypeMedication.Comprime, value: "comprime"},
    {key: TypeMedication.Inhalateur, value: "inhalateur"},
    {key: TypeMedication.Injectable, value: "injectable"},
    {key: TypeMedication.Liquide, value: "liquide"}
  ]

  getIcone(type: TypeMedication): string {
    return this.types[type].value + ".svg"
  }

  getType(type: TypeMedication): string {
    return this.types[type].value
  }
}
