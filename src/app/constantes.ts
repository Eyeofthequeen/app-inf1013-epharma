export enum TypeMedication {
    Comprime,
    Capsule,
    Injectable,
    Inhalateur,
    Liquide
}