import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { AdministrationComponent } from './pages/administration/administration.component';
import { InventaireComponent } from './pages/inventaire/inventaire.component';

const routes: Routes = [
    { path: '', component: InventaireComponent},
    { path: 'connexion', component: ConnexionComponent},
    { path: 'administration', component: AdministrationComponent}
  ];

@NgModule({
declarations: [],
imports: [
    CommonModule,
    RouterModule.forRoot(routes)
],
exports: [RouterModule]
})
export class AppRoutingModule { }