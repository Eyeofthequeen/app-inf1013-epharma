import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ConnexionComponent } from './pages/connexion/connexion.component';
import { AdministrationComponent } from './pages/administration/administration.component';
import { CarteComponent } from './components/carte/carte.component';
import { InventaireComponent } from './pages/inventaire/inventaire.component';
import { BoutonComponent } from './components/bouton/bouton.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ConnexionComponent,
    AdministrationComponent,
    CarteComponent,
    InventaireComponent,
    BoutonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
